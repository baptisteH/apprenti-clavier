import React, { useState, useEffect } from 'react';
import { timerFormat } from '../utils/helpers';

function Timer( { startTime } ) {
    
    const [ timer, setTimer ] = useState(Date.now() - startTime);
    
    const time = setTimeout(() => {
        setTimer(Date.now() - startTime);
    }, 1000);
    
    useEffect(() => {
        // Clear time if unmounted component
        return () => clearTimeout(time);
    });
    
    return (
        <>{ timerFormat(Math.floor(timer / 1000)) }</>
    );
}

export default Timer;
