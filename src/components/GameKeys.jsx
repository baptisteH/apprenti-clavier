import React, { useEffect, useState } from 'react';
import { ignoredKeys, getRandomKeys } from '../utils/mockGameService';

function GameKeys( { difficulty, onKeyStroke, onError, onGameEnd } ) {
    
    const keysNumber = 10;
    
    const [ keys, setKeys ] = useState(getRandomKeys(keysNumber, difficulty));
    const [ currentKeyIndex, setCurrentKeyIndex ] = useState(0);
    const [ inputValue, setInputValue ] = useState('');
    const [ inputState, setInputState ] = useState('');
    const [ inputPause, setInputPause ] = useState(false);
    
    const inputChange = ( event ) => {
        
        // If not an ignored key event
        if ( !ignoredKeys.includes(event.key) && !inputPause ) {
            onKeyStroke();
            
            if ( event.key === keys[currentKeyIndex] ) {
                setInputValue(inputValue + event.key);
                setInputState('success');
                setInputPause(true);
                
                setTimeout(function() {
                    nextKey();
                    setInputValue('');
                    setInputState('');
                    setInputPause(false);
                }, 500);
            }
            else {
                setInputValue(inputValue + event.key);
                setInputState('error');
                onError();
            }
        }
    };
    
    const nextKey = () => {
        if ( currentKeyIndex < keysNumber - 1 ) {
            setCurrentKeyIndex(currentKeyIndex + 1);
        }
        else {
            onGameEnd();
        }
    };
    
    const displayKey = ( key ) => {
        if ( key === ' ' ) {
            return '< Espace >';
        }
        return key;
    };
    
    const input = React.createRef();
    useEffect(() => {
        input.current.focus();
    });
    
    return (
        <div className="game-keys">
            <div className="game">
                <div className="key-container">
                    { displayKey(keys[currentKeyIndex]) }
                </div>
                <div className="input-container">
                    <input ref={ input } class={ inputState } type="text" value={ inputValue } onKeyDown={ inputChange } />
                </div>
            </div>
        </div>
    );
}

export default GameKeys;
