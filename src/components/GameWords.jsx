import React, { useEffect, useState } from 'react';
import { ignoredKeys, getRandomDictation } from '../utils/mockGameService';

function GameWords( { difficulty, onKeyStroke, onError, onGameEnd } ) {
    
    const [ dictation, setDictation ] = useState(getRandomDictation(difficulty));
    
    const [ currentKeyIndex, setCurrentKeyIndex ] = useState(0);
    
    const [ inputValue, setInputValue ] = useState('');
    const [ inputError, setInputError ] = useState(false);
    
    const inputChange = ( event ) => {
        
        // If not an ignored key event
        if ( !ignoredKeys.includes(event.key) ) {
            onKeyStroke();
            
            if ( event.key === dictation.charAt(currentKeyIndex) ) {
                nextKey();
                setInputValue(inputValue + event.key);
                setInputError(false);
            }
            else {
                onError();
                setInputError(true);
            }
        }
    };
    
    const nextKey = () => {
        if ( currentKeyIndex < dictation.length - 1 ) {
            setCurrentKeyIndex(currentKeyIndex + 1);
        }
        else {
            onGameEnd();
        }
    };
    
    const input = React.createRef();
    useEffect(() => {
        input.current.focus();
    });
    
    return (
        <div className="game-words">
            <div className="dictation-container">
                { dictation }
            </div>
            <div className="input-container">
                <textarea ref={ input } className={ inputError ? 'error' : '' } value={ inputValue } onKeyDown={ inputChange } />
            </div>
        </div>
    );
}

export default GameWords;
