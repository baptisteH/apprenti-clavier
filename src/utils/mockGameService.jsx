const letters = [ "a", "z", "e", "r", "t", "y", "u", "i", "o", "p", "q", "s", "d", "f", "g", "h", "j", "k", "l", "m", "w", "x", "c", "v", "b", "n" ];

const numbers = [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" ];

const special_chars = [ "&", "é", "'", "è", "ç", "à", "!", "?", ",", ".", ";", "/", ":", "+", "=", "ù", "%", "$", "*", "€", "-", " " ];

export const ignoredKeys = [ "Shift", "Alt", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight", "Meta", "Control", "Backspace", "CapsLock", "Tab", "Enter" ];

const dictations = {
    easy: [
        'Pas un nuage ne tranchait sur ce ciel invariable et morne comme l\'éternité.',
        'La mer, pendant son repos mystérieux et son sommeil, se dissimulait sous les teintes discrètes qui n\'ont pas de nom.',
    ],
    medium: [
        'Pourtant la nuit montait, pareille à une fumée sombre, et déjà comblait les vallées.',
        'Il lui semblait que l\'on se heurterait plus loin à l\'épaisseur de la nuit comme à un mur.',
    ],
    hard: [
        'Rien dans ce jardin ne contrariait l\'effort sacré des choses vers la vie ; la croissance vénérable était là chez elle.',
        'L\'iris dort, roulé en cornet sous une triple soie verdâtre, la pivoine perce la terre d\'une raide branche de corail vif, et le rosier n\'ose encore que des surgeons d\'un marron rose, d\'une vivante couleur de lombric..'
    ],
};

/**
 * Return random keys
 * @param number int
 * @param difficulty string
 * - easy
 * - medium
 * - hard
 */

export function getRandomKeys( number = 1, difficulty = 'easy' ) {
    
    let keys = [];
    
    // Depending on difficulty, keys list can include number and special chars
    switch ( difficulty ) {
        case 'easy' :
            keys = letters;
            break;
        case 'medium' :
            keys = letters.concat(numbers);
            break;
        case 'hard' :
            keys = letters.concat(numbers, special_chars);
            break;
    }
    
    let randomKeys = [];
    for ( let i = 0; i < number; i++ ) {
        let randomKey = keys[Math.floor(Math.random() * keys.length)];
        randomKeys.push(randomKey);
    }
    return randomKeys;
}

/**
 * Return random dictation
 * @param difficulty string
 * - easy
 * - medium
 * - hard
 */

export function getRandomDictation( difficulty = 'easy' ) {
    
    const dictationsList = dictations[difficulty];
    const randomDictationIndex = Math.floor(Math.random() * dictationsList.length);
    return dictationsList[randomDictationIndex];
}
