import React, { useContext, useState } from 'react';
import { timerFormat } from '../utils/helpers';

import GameContext from '../contexts/GameContext';

import GameKeys from '../components/GameKeys';
import GameWords from '../components/GameWords';
import Timer from '../components/Timer';
import { Link } from 'react-router-dom';

function GamePage() {
    const { gameContext } = useContext(GameContext);
    
    // End game
    const [ inGame, setInGame ] = useState(true);
    
    // Score
    const [ keySrokesNumber, setKeySrokesNumber ] = useState(0);
    const [ errors, setErrors ] = useState(0);
    
    // Timer
    const [ startTime, setStartTime ] = useState(Date.now());
    const [ endTime, setEndTime ] = useState(Date.now());
    
    // Stats
    const duration = Math.floor((endTime - startTime) / 1000);
    const keySrokesSpeed = Math.floor(keySrokesNumber / (duration / 60));
    const accuracy = keySrokesNumber > 1 ? Math.floor(((keySrokesNumber - errors) / keySrokesNumber) * 100) : 100;
    
    
    const endGame = () => {
        setInGame(false);
        setEndTime(Date.now());
    };
    
    return (
        <div>
            { inGame ?
                <div className="gamepage container">
                    <div className="game-container">
                        { gameContext.gameType === 'characters' ?
                            <GameKeys
                                difficulty={ gameContext.difficulty }
                                onKeyStroke={ () => setKeySrokesNumber(keySrokesNumber + 1) }
                                onError={ () => setErrors(errors + 1) }
                                onGameEnd={ () => endGame() }
                            />
                            :
                            <GameWords
                                difficulty={ gameContext.difficulty }
                                onKeyStroke={ () => setKeySrokesNumber(keySrokesNumber + 1) }
                                onError={ () => setErrors(errors + 1) }
                                onGameEnd={ () => endGame() }
                            />
                        }
                    </div>
                    <div className="indicators">
                        <div className="accuracy">
                            Précision : { accuracy }%
                        </div>
                        <div className="timer">
                            <Timer startTime={ startTime } />
                        </div>
                    </div>
                </div>
                :
                <div className="game-end container">
                    <h1>
                        Fin de partie
                    </h1>
                    <p>Durée : { timerFormat(duration) } minutes</p>
                    <p>Vitesse de frappe : { keySrokesSpeed } par minute </p>
                    <p>Précision : { accuracy } %</p>
                    <p>
                        <Link to="/">Nouvelle partie</Link>
                    </p>
                </div>
            }
        </div>
    );
}

export default GamePage;
