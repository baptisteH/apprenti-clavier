import React, { useState } from 'react';

// Create keycodes array
function KeyCodesGenerator() {
    
    const [ keycodes, setKeycodes ] = useState([]);
    
    const addKeyCode = ( e ) => {
        // Push new keycode in temp array
        let tempKeycodes = keycodes;
        tempKeycodes.push(e.key);
        // Remove duplicate keycodes
        tempKeycodes = tempKeycodes.filter(( item, index ) => tempKeycodes.indexOf(item) === index);
        console.log(tempKeycodes);
        setKeycodes(tempKeycodes);
    };
    
    return (
        <div className="container">
            <input type="text" onKeyDown={ addKeyCode } />
        </div>
    );
}

export default KeyCodesGenerator;
