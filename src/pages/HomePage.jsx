import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';

import GameContext from '../contexts/GameContext';

function HomePage() {
    
    const { gameContext, setGameContext } = useContext(GameContext);
    
    const checkboxChange = ( property, value ) => {
        
        const gameType = property === 'gameType' ? value : gameContext.gameType;
        const difficulty = property === 'difficulty' ? value : gameContext.difficulty;
        
        setGameContext({
            'gameType': gameType,
            'difficulty': difficulty,
        });
    };
    
    const displayGameType = ( gameType ) => {
        switch ( gameType ) {
            case 'characters':
                return 'Caractères';
                break;
            case 'words':
                return 'Mots';
                break;
        }
    };
    const displayDifficulty = ( difficulty ) => {
        switch ( difficulty ) {
            case 'easy':
                return 'Facile';
                break;
            case 'medium':
                return 'Moyenne';
                break;
            case 'hard':
                return 'Difficile';
                break;
        }
    };
    
    return (
        <div className="homepage container">
            <h1>Apprenti Clavier</h1>
            <div className="indications">
                <p>Mode : { displayGameType(gameContext.gameType) }</p>
                <p>Difficulté : { displayDifficulty(gameContext.difficulty) }</p>
            </div>
            <div className="options">
                <div className="option game-type">
                    <p>Type</p>
                    <div>
                        <input type="radio" name="game-type" id="characters"
                               checked={ gameContext.gameType === 'characters' }
                               onChange={ () => checkboxChange('gameType', 'characters') } />
                        <label htmlFor="characters">Caractères</label>
                    </div>
                    <div>
                        <input type="radio" name="game-type" id="words"
                               checked={ gameContext.gameType === 'words' }
                               onChange={ () => checkboxChange('gameType', 'words') } />
                        <label htmlFor="words">Mots</label>
                    </div>
                </div>
                <div className="option game-difficulty">
                    <p>Difficulté</p>
                    <div>
                        <input type="radio" name="difficulty" id="easy"
                               checked={ gameContext.difficulty === 'easy' }
                               onChange={ () => checkboxChange('difficulty', 'easy') } />
                        <label htmlFor="easy">Facile</label>
                    </div>
                    <div>
                        <input type="radio" name="difficulty" id="medium"
                               checked={ gameContext.difficulty === 'medium' }
                               onChange={ () => checkboxChange('difficulty', 'medium') } />
                        <label htmlFor="medium">Moyenne</label>
                    </div>
                    <div>
                        <input type="radio" name="difficulty" id="hard"
                               checked={ gameContext.difficulty === 'hard' }
                               onChange={ () => checkboxChange('difficulty', 'hard') } />
                        <label htmlFor="hard">Difficile</label>
                    </div>
                </div>
            </div>
            <Link to="/game">Démarrer</Link>
        </div>
    );
}

export default HomePage;
