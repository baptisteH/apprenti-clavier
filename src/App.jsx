import React, { useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';

import HomePage from './pages/HomePage';
import GamePage from './pages/GamePage';
import KeyCodesGenerator from './pages/KeyCodesGenerator';
import GameContext from './contexts/GameContext';

function App() {
    const [ gameContext, setGameContext ] = useState({
        'gameType': 'characters',
        'difficulty': 'easy',
    });
    const value = { gameContext, setGameContext };
    
    return (
        <GameContext.Provider value={ value }>
            <Router>
                <div id="App">
                    <Switch>
                        <Route path="/keycodes-generator">
                            <KeyCodesGenerator />
                        </Route>
                        <Route path="/game">
                            <GamePage />
                        </Route>
                        <Route path="/">
                            <HomePage />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </GameContext.Provider>
    );
}

export default App;
