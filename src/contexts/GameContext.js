import React from 'react';

/**
 * Game parameters
 * @param gameType string
 * - characters
 * - words
 * @param difficulty string
 * - easy
 * - medium
 * - hard
 */

const GameContext = React.createContext({
    gameContext: {
        'gameType': 'characters',
        'difficulty': 'easy',
    },
    setGameContext: () => {}
});

export default GameContext;
